//MovieListNode.h

//Authors:
//		Haydn Brindley n8318824
//		Alexander Crichton n6878296
//Date:
//		27/05/2012
//Unit:
//		INB371

#pragma once
#include "stdafx.h"



// the MovieListNode class represents a single node in a list of movies.
// each node points to a movie and the next node in the list
class MovieListNode
{	
	public:
		//pre:  true
		//post: an object of CustomerCollectionNode is created
		// *data and *next are unassigned
		MovieListNode();

		//pre:  true
		//post: node is destroyed and its allocated memory is reclaimed
		~MovieListNode();	

		Movie *data; // points to a customer

		MovieListNode *next; // points to the next node or zero if 
									  // there isn't one

}; //--- end of MovieListNode 