//Customer.cpp

#pragma once
#include "stdafx.h"
#include "Customer.h"





//--- definition for Customer()
Customer::Customer()
{
}





//--- definition for Customer()
Customer::Customer(const string lastName, 
	const string firstName, const string address, const string phoneNo, const int password): myLName(lastName), 
	myFName(firstName), myAddress(address), myPhoneNo(phoneNo), myPword(password), myTopMoviesRents()
{
	for (int i = 0; i < 10; i++)
	{
		myTopMoviesRents[i] = 0;
	}
}





//--- definition for getMyLName()
string Customer::getMyLName()
{
	return myLName;
}





//--- definition for getMyFName()
string Customer::getMyFName()
{
	return myFName;
}





//--- definition for getMyPhoneNo()
string Customer::getMyPhoneNo()
{
	return myPhoneNo;
}





//--- definition for displayName()
void Customer::displayName()
{
	cout << myLName + " " + myFName << endl;
}





//--- definition for getMyPword()
int Customer::getMyPword()
{
	return myPword;
}





//--- definition for addMovie()
void Customer::addMovie(Movie *item)
{
	myMovies.insert(item);
}





//--- definition for removeMovie()
void Customer::removeMovie(Movie *item)
{
	myMovies.remove(item, false);
}





//---definition for checkRenting()
bool Customer::checkRenting(Movie *item)
{
	return myMovies.search(*item);
}





//--- defintion for showMyRentals()
void Customer::showMyRentals()
{
	myMovies.preOrderTraverse();
}





//--- definition for showTopTen()
void Customer::showTopTen()
{
	stringstream tempStream;
	string tempLine;
	cout<<"\n\nYour top ten movies\n";
	for (int i = 0; i < 10; i++)
	{
		cout<<"\n\n" + myTopMoviesTitles[i];
		tempStream << myTopMoviesRents[i];
		tempStream >> tempLine;
		tempStream.str("");
		tempStream.clear();
		cout<<"\t" + tempLine;
	}
}





//--- definition for doTopTen
void Customer::doTopTen(string movieName)
{
	bool inList = false;
	bool done = false;

	for (int i = 0; i < 10; i++)
	{
		if (!done)
		{
			if (myTopMoviesTitles[i] == movieName)
			{
				inList = true;
				myTopMoviesRents[i]++;
				done = true;
			}
			else if (myTopMoviesRents[i] == 0)
			{
				myTopMoviesTitles[i] = movieName;
				myTopMoviesRents[i] = 1;
				done = true;
				inList = true;
			}
		}
	}

	if (!inList)
	{
		if (myTopMoviesRents[9] = 1)
		{
			myTopMoviesTitles[9] = movieName;
		}
	}

	for (int i = 0; i < 9; i++)
	{
		string tempTitle;
		int tempRents;

		if (myTopMoviesRents[i] < myTopMoviesRents[i + 1])
		{
			tempTitle = myTopMoviesTitles[i];
			tempRents = myTopMoviesRents[i];
			myTopMoviesTitles[i] = myTopMoviesTitles[i + 1];
			myTopMoviesRents[i] = myTopMoviesRents[i + 1];
			myTopMoviesTitles[i + 1] = tempTitle;
			myTopMoviesRents[i + 1] = tempRents;
		}
	}
}





//--- defition for ~Customer()
Customer::~Customer()
{
}