//CustomerCollection.cpp

#pragma once
#include "stdafx.h"
#include "Customer.h"
#include "CustomerCollection.h"
#include "CustomerCollectionNode.h"





//--- definition for CustomerCollection()
CustomerCollection::CustomerCollection()
{ 
   first = 0; 
   mySize = 0;
}





//--- definition for insert()
void CustomerCollection::insert(Customer *newCustomer)
{
	// special case
	// if new customer needs to be inserted at the front of the list
	// because it is empty
	if (mySize == 0)
	{ 
		CustomerCollectionNode *newCustomerNode = new CustomerCollectionNode();

		first = newCustomerNode;             // first now points to the new node

		newCustomerNode->data = newCustomer; // the new node's data points to the new customer
		newCustomerNode->next = 0;           // the new node's next points to zero
	}

	// special case
	// if new customer needs to be inserted at the front of the list
	// but the list is not empty
	else if (newCustomer->getMyLName().compare(first->data->getMyLName()) < 0)
	{ 
		CustomerCollectionNode *newCustomerNode = new CustomerCollectionNode();

		newCustomerNode->data = newCustomer; // the new node's data points to the new customer
		newCustomerNode->next = first;		 // the new node's next points to the original node

		first = newCustomerNode;             // first now points to the new node		
	}

	// general case
	else
	{
		CustomerCollectionNode *currNode = first; // *currNode points to first node in list
		CustomerCollectionNode *prevNode = 0;     // *prevNode points to zero

		// iterate through the entire list to the correct position
		for (int i = 0; i < mySize; i++)
		{  
			prevNode = currNode;			// the previous node's pointer becomes the current node's pointer

			if (currNode->next != 0)		// if the current node is not the last one
				currNode = currNode->next;  // the current node's pointer points to the next node		

			if (newCustomer->getMyLName().compare(currNode->data->getMyLName()) < 0) // check whether to insert new customer at the current position
				break;	
		}
		CustomerCollectionNode *newCustomerNode = new CustomerCollectionNode();

		prevNode->next = newCustomerNode;     // the previous node's next points to the new node
			
		newCustomerNode->data = newCustomer;  // the new node's data points to the new customer
		newCustomerNode->next = currNode;     // the new node's next points to the current node
	}
	mySize++;
}





//--- definition for erase()
void CustomerCollection::erase(CustomerCollectionNode *removeCustomerNode)
{
	// if the list is empty, display message
	if (mySize == 0)
	{
		cout << "No customers in list";
		return;
	}

	else
	{
		CustomerCollectionNode *currNode = first->next; // currNode points to the first node in the list
		CustomerCollectionNode *prevNode = first;		// prevNode 

		// iterate through the list until the specified node is found
		for (int i = 0; i < mySize; i++)
		{
			if (currNode = removeCustomerNode)
			{
				prevNode->next = currNode->next; // the previous node's next now points to the next node
				delete removeCustomerNode;		 // destroy the current node
				mySize--;
				return;
			}
			prevNode = currNode;
			currNode = currNode->next;
		}
		// if the specified node isn't found, display message
		cout << endl << "no match";
		return;
	}
}





//--- definition for getCustomerNode()
CustomerCollectionNode *CustomerCollection::getCustomerNode(string tempLastName, string tempFirstName)
{
	CustomerCollectionNode *customerNode = first;

	// iterate through the list until a customer is found
	// with the specified first and last names
	for (int i = 0; i < mySize; i++)
	{
		if ((customerNode->data->getMyLName() == tempLastName)
			 &&
			(customerNode->data->getMyFName() == tempFirstName))
		{
			return customerNode;
		}
		customerNode = customerNode->next;
	}
	// if the specified customer isn't found, display message
	cout << "No match";
	return customerNode;
}





//--- definition for empty()
bool CustomerCollection::empty() const
{ 
   return (first == 0); 
}





//--- definition for getMySize()
int CustomerCollection::getMySize()
{
	 return mySize;
}





//--- definition for getPhoneNo()
string CustomerCollection::getPhoneNo(string tempLastName, string tempFirstName)
{

	CustomerCollectionNode *ptr = first;

	// iterate through the list until a customer is found
	// with the specified first and last names
	for (int i = 0; i < mySize; i++)
	{
		if ((ptr->data->getMyLName() == tempLastName)
			&&
			(ptr->data->getMyFName() == tempFirstName))
		{
			return ptr->data->getMyPhoneNo();
		}
		ptr = ptr->next;
	}
	return "error";
}





//--- definition for getLoginInfo()
int CustomerCollection::getLoginInfo(string userName)
{
	CustomerCollectionNode *ptr = first;

	// iterate through the list until a customer is found
	// with the specified first and last names
	for (int i = 0; i < mySize; i++)
	{
		if ( (ptr->data->getMyLName() + ptr->data->getMyFName() ) == userName)
		{
			return ptr->data->getMyPword();
		}
		ptr = ptr->next;
	}
	return -1;
}





//--- definition for RentDvd()
void CustomerCollection::rentDVD(string userName, Movie *item)
{
	CustomerCollectionNode *ptr = first;

	for (int i = 0; i < mySize; i++)
	{

		if ((ptr->data->getMyLName() + ptr->data->getMyFName()) == userName)
		{
			ptr->data->addMovie(item);
			ptr->data->doTopTen(item->getTitle());
			return;
		}
		ptr = ptr->next;
	}
}





//--- definition for returnDVD
void CustomerCollection::returnDVD(string userName, Movie *item)
{
	CustomerCollectionNode *ptr = first;

	for (int i = 0; i < mySize; i++)
	{

		if ((ptr->data->getMyLName() + ptr->data->getMyFName()) == userName)
		{
			ptr->data->removeMovie(item);
			return;
		}
		ptr = ptr->next;
	}
}





//--- definition for displayCustomers()
void CustomerCollection::displayCustomers()
{
	CustomerCollectionNode *currNode = first;

	for (int i = 0; i < mySize; i++)
	{
		currNode->data->displayName();

		currNode = currNode->next;
	}
}





//--- definition for searchRenting()
void CustomerCollection::searchRenting(Movie *item)
{
	CustomerCollectionNode *currNode = first;

	for (int i = 0; i < mySize; i++)
	{
		if (currNode->data->checkRenting(item))
		{
			currNode->data->displayName();
		}

		currNode = currNode->next;
	}
}





//--- definition for checkRenting()
bool CustomerCollection::checkRenting(string userName, Movie *item)
{
	CustomerCollectionNode *currNode = first;

	for (int i = 0; i < mySize; i++)
	{
		if ((currNode->data->getMyLName() + currNode->data->getMyFName()) == userName)
		{
			return currNode->data->checkRenting(item);
		}
		currNode = currNode->next;
	}
}





//--- definition for showRentals()
void CustomerCollection::showRentals(string userName)
{
	CustomerCollectionNode *ptr = first;

	for (int i = 0; i < mySize; i++)
	{

		if ((ptr->data->getMyLName() + ptr->data->getMyFName()) == userName)
		{
			ptr->data->showMyRentals();
			return;
		}
		ptr = ptr->next;
	}
}





//--- definition for showTopTen()
void CustomerCollection::showTopTen(string userName)
{
	CustomerCollectionNode *ptr = first;

	for (int i = 0; i < mySize; i++)
	{

		if ((ptr->data->getMyLName() + ptr->data->getMyFName()) == userName)
		{
			ptr->data->showTopTen();
			return;
		}
		ptr = ptr->next;
	}
}





//--- definition for CustomerCollection()
CustomerCollection::~CustomerCollection()
{     
   // Set pointers to run through the CustomerCollection
   CustomerCollectionNode *currPtr = first,  // node to be deallocated
						  *nextPtr;          // its successor
   while (currPtr != 0)
   {
	   nextPtr = currPtr->next;
       delete currPtr;
       currPtr = nextPtr;
   }
   cout <<"destructor \n";
}