//CustomerCollectionNode.h

//Authors:
//		Haydn Brindley n8318824
//		Alexander Crichton n6878296
//Date:
//		27/05/2012
//Unit:
//		INB371

#pragma once
#include "stdafx.h"



// the CustomerCollectionNode class represents a node in a list of movies.
// each node points to a movie and also the next node in the list
class CustomerCollectionNode
{	
	public:
		//pre:  true
		//post: an object of CustomerCollectionNode is created
		// *data and *next are unassigned
		CustomerCollectionNode();

		//pre:  true
		//post: node is destroyed and its allocated memory is reclaimed
		~CustomerCollectionNode();	

		Customer *data; // points to a customer

		CustomerCollectionNode *next; // points to the next node or zero if 
									  // there isn't one

}; //--- end of CustomerCollectionNode class