//Movie.cpp

#pragma once
#include "stdafx.h"
#include "Movie.h"





//--- definition for Movie()
Movie::Movie(const string title, const int duration, const string starring, const string director, const string genre,
		const string classification, const int releaseDate, const int noDvds, const int totalDvds): myTitle(title), myDuration(duration),
	myStarring(starring), myDirector(director), myGenre(genre), myClassification(classification), myReleaseDate(releaseDate), myNoDvds(noDvds), myTotalDvds(totalDvds)
{
}





//--- definition for getTitle()
string Movie::getTitle() const
{
	return myTitle;
}





//--- definition for getDuration()
int Movie::getDuration() const
{
	return myDuration;
}





//--- definition for getStarring()
string Movie::getStarring() const
{
	return myStarring;
}





//--- definition for getDirector()
string Movie::getDirector() const
{
	return myDirector;
}





//--- definition for getGenre()
string Movie::getGenre() const
{
	return myGenre;
}





//--- definition for getClassification()
string Movie::getClassification() const
{
	return myClassification;
}





//--- definition for getReleaseDate()
int Movie::getReleaseDate() const
{
	return myReleaseDate;
}
	




//--- definition for getNoDvds()
int Movie::getNoDvds() const
{
	return myNoDvds;
}





//--- definition for getTotalDvds()
int Movie::getTotalDvds() const
{
	return myTotalDvds;
}





//--- definition for setDvds()
void Movie::setDvds(const int noDvdAdd)
{
	myNoDvds = myNoDvds + noDvdAdd;
}





//--- definition for setDvdsTotal()
void Movie::setDvdsTotal(const int noDvdAdd)
{
	myNoDvds = myNoDvds + noDvdAdd;
	myTotalDvds = myTotalDvds + noDvdAdd;
}





//--- definition for checkNoRents()
bool Movie::checkNoRents() const
{
	if (myNoDvds == myTotalDvds)
		return true;
	else
		return false;
}





//--- definition for getInfo()
void Movie::getInfo() const
{
	stringstream stream;
	string tempString;

	cout<<"\nTitle: " + myTitle;

	stream << myDuration;
	stream >> tempString;
	stream.str("");
	stream.clear();
	cout<<"\nDuration: " + tempString + " mins";

	cout<<"\nStarring: " + myStarring;

	cout<<"\nDirector: " + myDirector;

	cout<<"\nGenre: " + myGenre;

	cout<<"\nClassification: " + myClassification;

	stream << myReleaseDate;
	stream >> tempString;
	stream.str("");
	stream.clear();
	cout<<"\nRelease Date: " + tempString;

	stream << myNoDvds;
	stream >> tempString;
	stream.str("");
	stream.clear();
	cout<<"\nAvailable DVDs: " + tempString;

	stream << myTotalDvds;
	stream >> tempString;
	stream.str("");
	stream.clear();
	cout<<"\nTotal DVDs: " + tempString;
}





//--- definition for checkOutDvd()
bool Movie::checkOutDvd()
{
	if (myNoDvds > 0)
	{
		myNoDvds = myNoDvds - 1;
		return true;
	}
	else
		return false;
}
