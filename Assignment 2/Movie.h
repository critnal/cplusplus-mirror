//Movie.h

//Authors:
//		Haydn Brindley n8318824
//		Alexander Crichton n6878296
//Date:
//		27/05/2012
//Unit:
//		INB371

#pragma once
#include "stdafx.h"



// the Movie class represents a single movie
class Movie
{

public:
	//pre:  true
	//post: creates an object of Movie
	Movie(const string title, const int duration, const string starring, const string director, const string genre,
		const string classification, const int releaseDate, const int noDvds, const int totalDvds);
	
	//pre:  true
	//post: returns the movie's title
	string getTitle() const;

	//pre: true
	//post: return the movie's duration
	int getDuration() const;

	//pre: true
	//post: return the movie's starring
	string getStarring() const;

	//pre: true
	//post: return the movie's director
	string getDirector() const;

	//pre: true
	//post: return the movie's genre
	string getGenre() const;

	//pre: true
	//post: return the movie's classification
	string getClassification() const;

	//pre: true
	//post: return the movie's release dat
	int getReleaseDate() const;
	
	//pre: true
	//post: return the movie's number of dvds
	int getNoDvds() const;

	//pre: true
	//post: return the movie's total number of dvds
	int getTotalDvds() const;

	//pre:  true
	//post: adds the specified number of dvds to the movie's
	// current dvd count
	void setDvds(const int noDvdAdd);

	//pre:  true
	//post: adds the specified number of dvds to the movie's
	// total dvd count
	void setDvdsTotal(const int noDvdAdd);

	//pre:  true
	//post: returns true if the movie's number of available dvds
	// equals its number of total dvds
	bool checkNoRents() const;

	//pre:  true
	//post: displays the movie's information
	void getInfo() const;

	//pre:  true
	//post: returns true if this movie has at least one DVD
	bool checkOutDvd();



private:
	string myTitle;

	int myDuration;
	
	string myStarring; // star actors in the movie
	
	string myDirector;
	
	string myGenre;
	
	string myClassification;
	
	int myReleaseDate;
	
	int myNoDvds; // the number of dvds available for this movie
	
	int myTotalDvds; // the total number of dvds for this movie

}; //--- end of Movie class
