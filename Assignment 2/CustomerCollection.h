//CustomerCollection.h

//Authors:
//		Haydn Brindley n8318824
//		Alexander Crichton n6878296
//Date:
//		27/05/2012
//Unit:
//		INB371

#pragma once
#include "stdafx.h"
#include "Customer.h"
#include "CustomerCollectionNode.h"





// the CustomerCollection class represents a single list of movies.
// the list contains node objects which point to movies
class CustomerCollection
{
	public:
		//pre:  true
		//post: an object of CustomerCollection is created
		// mySize is set to zero
		// first is set to zero
		CustomerCollection();
	
		//pre:  true
		//post: returns true if the list contains zero nodes
		// otherwise returns false
		bool empty() const;

		//pre:  true
		//post: inserts a customer into the list
		void insert(Customer *newCustomer);

		//pre:  true
		//post: locates a specified node in the list,
		// links the previous node to the next node,
		// destroys the current node
		void erase(CustomerCollectionNode *removeCustomerNode);

		//pre:  true
		//post: returns the size of the list
		int getMySize();

		//pre:  true
		//post: locates the first node in the list that points to a customer
		// with the specified first and last names.
		// returns a pointer to that node
		CustomerCollectionNode *getCustomerNode(string tempLastName, string tempFirstName);

		//pre:  true
		//post: locates the first node in the list that points to a customer
		// with the specified first and last names.
		// returns that customer's phone number
		string getPhoneNo(string tempLastName, string tempFirstName);

		//pre:  true
		//post: locates the first node in the list that points to a customer
		// with the specified name.
		// returns that customer's password
		int getLoginInfo(string userName);

		//pre:  true
		//post: add item to userName's MovieCollection
		void rentDVD(string userName, Movie *item);

		//pre:  true
		//post: remove item from userName's MovieCollection
		void returnDVD(string userName, Movie *item);

		//pre:  true
		//post: displays the name of every customer in CustomerCollection
		void displayCustomers();

		//pre:  true
		//post: displays the name of every customer in CustomerCollection
		// who is renting item
		void searchRenting(Movie *item);

		//pre:  true
		//post: returns true if userName is renting item
		bool checkRenting(string userName, Movie *item);

		//pre:  true
		//post: displays current rented movies in userName's MovieCollection
		void showRentals(string userName);

		//pre:  true
		//post: displays userName's top ten movies
		void showTopTen(string userName);

		//pre:  true
		//post: list is destroyed and its allocated memory is reclaimed
		~CustomerCollection();



	private:
		int mySize; // number of nodes currently in the CustomerCollection

		CustomerCollectionNode *first; // points to first node in the list

}; //--- end of CustomerCollection class 
