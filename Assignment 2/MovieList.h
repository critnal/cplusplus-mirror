//MovieList.h

//Authors:
//		Haydn Brindley n8318824
//		Alexander Crichton n6878296
//Date:
//		27/05/2012
//Unit:
//		INB371

#pragma once
#include "stdafx.h"
#include "Movie.h"
#include "MovieListNode.h"



// the MovieList class represents a single list of movies.
// this class exists to collect movies in alphabetical order
class MovieList
{
	public:
		//pre:  true
		//post: an object of CustomerCollection is created
		// mySize is set to zero
		// first is set to zero
		MovieList();
	
		//pre:  true
		//post: returns true if the list contains zero nodes
		// otherwise returns false
		bool empty() const;

		//pre:  true
		//post: inserts a customer into the list
		void insert(Movie *newMovie);

		//pre:  true
		//post: locates a specified node in the list,
		// links the previous node to the next node,
		// destroys the current node
		void erase(MovieListNode *removeMovieNode);

		//pre:  true
		//post: returns the size of the list
		int getMySize();

		//pre:  true
		//post: locates the first node in the list that points to a customer
		// with the specified first and last names.
		// returns a pointer to that node
		MovieListNode *getMovieNode(string title);

		//pre:  true
		//post: displays every Movie in MovieList
		void displayMovies();

		//pre:  true
		//post: list is destroyed and its allocated memory is reclaimed
		~MovieList();


	private:
		int mySize; // number of nodes currently in the CustomerCollection

		MovieListNode *first; // points to first node in the list

}; //--- end of MovieList class 
