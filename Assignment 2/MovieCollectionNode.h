//MovieCollectionNode.h

//Authors:
//		Haydn Brindley n8318824
//		Alexander Crichton n6878296
//Date:
//		27/05/2012
//Unit:
//		INB371

#pragma once
#include "stdafx.h"



// the MovieCollectionNode class represents a single node
// in a binary search tree.
// each node points to a movie and a left and right child
class MovieCollectionNode
{
	public:
		//pre:  true
		//post: an object of BTreeNode is created, the data of the node is 
		// initialised with Item, and the left and right child pointers are null.
		MovieCollectionNode(Movie *newItem);

		//pre:  true
		//post: return the left child pointer
		MovieCollectionNode* getLChild() const;

		//pre:  true
		//post: the left child pointer is replaced by p
		void setLChild(MovieCollectionNode * p);

		//pre:  true
		//post: return the right child pointer
		MovieCollectionNode* getRChild() const;

		//pre:  true
		//post: the right child pointer is replaced by p
		void setRChild(MovieCollectionNode * p);

		//pre:  true
		//post: destroy the node and reclaim the memory used by the node
		~MovieCollectionNode();

		Movie *item;	// node data



	private:		
		MovieCollectionNode * lchild; // left child pointer
		
		MovieCollectionNode * rchild; // right child pointer

}; //--- end of MovieCollectionNode class