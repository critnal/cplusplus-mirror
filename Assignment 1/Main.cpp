//Author:		Alexander Crichton
//Student ID:	n6878296
//Date:			02/04/2012
//Unit:			INB371

#include "Schedule.h" 
#include <iostream>
#include <string>
using namespace std;

//Function prototypes
void mainMenu();
void makeBookingMenu();
void makeBooking(int instructor, int day, int slot);
void cancelBookingMenu();
void cancelBooking(int instructor, int day, int slot);
void displayScheduleMenu();
void displaySchedule(int instructor);
void displayAvailableSlots();
void updateAvailableSlotsMenu();
void updateAvailableSlots(int instructor, int day, int slot);
void clearScheduleMenu();
void clearSchedule(int instructor);

//Static objects to be used in Main
Schedule jeff("Jeff");
Schedule anna("Anna");
Schedule peter("Peter");
Schedule michael("Michael");
Schedule kerry("Kerry");

Schedule scheduleArray[5] = {jeff, anna, peter, michael, kerry};


int main()
{
	mainMenu();

	cin.ignore();
	cin.get();
	return 0;
}



void mainMenu()
{
	int mainMenuOption = 0;

	do
	{
		cout << endl << endl << endl << endl << endl << "Swim Club Bookings"; 
		cout << endl << endl << endl;
		cout << "\tChoose an option:" << endl << endl;
		cout << "\t\t1. Book a swimming lesson" << endl;
		cout << "\t\t2. Cancel a swimming lesson" << endl;
		cout << "\t\t3. Display available slots" << endl;
		cout << "\t\t4. Update available slots" << endl;
		cout << "\t\t5. Display schedule" << endl;		
		cout << "\t\t6. Clear schedule" << endl;
		cout << "\t\t0. Exit" << endl << endl;

		cin >> mainMenuOption;

		switch (mainMenuOption)
		{
		case 1:
			makeBookingMenu();
			break;

		case 2:
			cancelBookingMenu();
			break;

		case 3:
			displayAvailableSlots();
			break;

		case 4:
			updateAvailableSlotsMenu();
			break;

		case 5:
			displayScheduleMenu();
			break;

		case 6:
			clearScheduleMenu();
			break;

		case 0:
			break;

		default:
			cout << endl << "Please enter a valid input.";
			cin.ignore();
			cin.get();
			break;
		}

	} while (mainMenuOption != 0);
}



void makeBookingMenu()
{
	int instructorOption = 0;
	int dayOption = 0;
	int slotOption = 0;

	do //Choose instructor to make booking with
	{
		cout << endl << endl << endl << "Make Booking Menu";
		cout << endl << endl << endl;
		cout << "\tChoose an instructor to book the lesson with:" << endl << endl;
		cout << "\t\t1. Jeff" << endl;
		cout << "\t\t2. Anna" << endl;
		cout << "\t\t3. Peter" << endl;
		cout << "\t\t4. Michael" << endl;
		cout << "\t\t5. Kerry" << endl;
		cout << "\t\t0. Back" << endl;

		cin >> instructorOption;

		if (instructorOption >= 1 && instructorOption <= 5)
		{			
			cout << endl << endl << endl;
			cout << "\tChoose a day to book the lesson:" << endl << endl;
			cout << "\t\t1. Monday" << endl;
			cout << "\t\t2. Tuesday" << endl;
			cout << "\t\t3. Wednesday" << endl;
			cout << "\t\t4. Thursday" << endl;
			cout << "\t\t5. Friday" << endl;
			cout << "\t\t0. Back" << endl;
		
			cin >> dayOption;
		
			if (dayOption >= 1 && dayOption <= 5)
			{					
				cout << endl << endl << endl;
				cout << "\tChoose a slot to book the lesson:" << endl << endl;
				cout << "\t\t1. 9am - 10am" << endl;
				cout << "\t\t2. 10am - 11am" << endl;
				cout << "\t\t3. 11am - 12pm" << endl;
				cout << "\t\t4. 1pm - 2pm" << endl;
				cout << "\t\t5. 2pm - 3pm" << endl;
				cout << "\t\t6. 3pm - 4pm" << endl;
				cout << "\t\t7. 4pm - 5pm" << endl;
				cout << "\t\t0. Back" << endl;

				cin >> slotOption;

				if (slotOption >= 1 && slotOption <= 7)
				{
					makeBooking(instructorOption - 1, dayOption - 1, slotOption - 1);
					return;
				} else cout << endl << "Please enter a valid input";
			} else cout << endl << "Please enter a valid input";
		} else cout << endl << "Please enter a valid input";
	} while (instructorOption != 0 && dayOption != 0 && slotOption != 0);
}



void makeBooking(int instructor, int day, int slot)
{
	switch (scheduleArray[instructor].getSlot(day, slot))
	{
	case SLOT_FREE:
		scheduleArray[instructor].setSlot(SLOT_BOOKED, day, slot);
		break;

	case SLOT_BOOKED:
		cout << "That slot has already been booked.";
		break;

	case SLOT_UNAVAILABLE:
		cout << "That slot is not available for that instructor.";
		break;

	default:
		break;
	}
}



void cancelBookingMenu()
{
	int instructorOption = 0;
	int dayOption = 0;
	int slotOption = 0;

	do //Choose instructor to cancel booking with
	{
		cout << endl << endl << endl << "Cancel Booking Menu";
		cout << endl << endl << endl;
		cout << "\tChoose an instructor to cancel a lesson:" << endl << endl;
		cout << "\t\t1. Jeff" << endl;
		cout << "\t\t2. Anna" << endl;
		cout << "\t\t3. Peter" << endl;
		cout << "\t\t4. Michael" << endl;
		cout << "\t\t5. Kerry" << endl;
		cout << "\t\t0. Back" << endl;

		cin >> instructorOption;

		if (instructorOption >= 1 && instructorOption <= 5)
		{			
			cout << endl << endl << endl;
			cout << "\tChoose a day to cancel the lesson:" << endl << endl;
			cout << "\t\t1. Monday" << endl;
			cout << "\t\t2. Tuesday" << endl;
			cout << "\t\t3. Wednesday" << endl;
			cout << "\t\t4. Thursday" << endl;
			cout << "\t\t5. Friday" << endl;
			cout << "\t\t0. Back" << endl;
		
			cin >> dayOption;
		
			if (dayOption >= 1 && dayOption <= 5)
			{					
				cout << endl << endl << endl;
				cout << "\tChoose a slot to cancel the lesson:" << endl << endl;
				cout << "\t\t1. 9am - 10am" << endl;
				cout << "\t\t2. 10am - 11am" << endl;
				cout << "\t\t3. 11am - 12pm" << endl;
				cout << "\t\t4. 1pm - 2pm" << endl;
				cout << "\t\t5. 2pm - 3pm" << endl;
				cout << "\t\t6. 3pm - 4pm" << endl;
				cout << "\t\t7. 4pm - 5pm" << endl;
				cout << "\t\t0. Back" << endl;

				cin >> slotOption;

				if (slotOption >= 1 && slotOption <= 7)
				{
					cancelBooking(instructorOption - 1, dayOption - 1, slotOption - 1);
					return;
				} else cout << endl << "Please enter a valid input";
			} else cout << endl << "Please enter a valid input";
		} else cout << endl << "Please enter a valid input";
	} while (instructorOption != 0 && dayOption != 0 && slotOption != 0);
}



void cancelBooking(int instructor, int day, int slot)
{
	switch (scheduleArray[instructor].getSlot(day, slot))
	{
	case SLOT_BOOKED:
		scheduleArray[instructor].setSlot(SLOT_FREE, day, slot);
		break;

	case SLOT_FREE:
		cout << "That slot currently has no booking.";
		break;

	case SLOT_UNAVAILABLE:
		cout << "That slot is not available for that instructor.";
		break;

	default:
		break;
	}
}



void displayScheduleMenu()
{
	int displayScheduleMenuOption = 0;

	do
	{
		cout << endl << endl << endl << "Display Schedule Menu";
		cout << endl << endl << endl;
		cout << "\t Choose an instructor's schedule to display:" << endl << endl;
		cout << "\t\t1. Jeff" << endl;
		cout << "\t\t2. Anna" << endl;
		cout << "\t\t3. Peter" << endl;
		cout << "\t\t4. Michael" << endl;
		cout << "\t\t5. Kerry" << endl;
		cout << "\t\t6. All instructor's schedules" << endl;
		cout << "\t\t0. Back" << endl << endl;

		cin >> displayScheduleMenuOption;

		switch (displayScheduleMenuOption)
		{
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
			displaySchedule(displayScheduleMenuOption - 1);
			cout << endl << "Press any key to continue...";
			cin.ignore();
			cin.get();
			break;

		case 6:
			for (int instructor = 0; instructor < 5; instructor++)
			{
				displaySchedule(instructor);						
			}
			cout << endl << "Press any key to continue...";
			cin.ignore();
			cin.get();
			break;

		case 0:
			break;

		default:
			cout << endl << "Please enter a valid input.";
			cin.ignore();
			cin.get();
			break;
		}
	} while (displayScheduleMenuOption != 0);
}



void displaySchedule(int instructor)
{	
	cout << endl << endl;
	cout << endl << "======================================================================";
	cout << endl << scheduleArray[instructor].getName() << "\tMon\tTue\tWed\tThur\tFri";
	cout << endl << "----------------------------------------------------------------------";

	for (int slot = 0; slot < SLOTSINDAY; slot++)
	{
		cout << endl << (slot + 9);

		for (int day = 0; day < DAYSINWEEK; day++)
		{
			cout << "\t";
			if (scheduleArray[instructor].getSlot(day, slot) == SLOT_BOOKED ||
				scheduleArray[instructor].getSlot(day, slot) == SLOT_UNAVAILABLE)
				cout << "x";
		}
	}
	cout << endl << "----------------------------------------------------------------------";	
}



void displayAvailableSlots()
{
	cout << endl;
	cout << endl << "======================================================================";
	cout << endl << "\tMon\tTue\tWed\tThur\tFri";
	cout << endl << "----------------------------------------------------------------------";

	for (int slot = 0; slot < SLOTSINDAY; slot++)
	{
		cout << endl << (slot + 9);

		for (int day = 0; day < DAYSINWEEK; day++)
		{
			cout << "\t";

			for (int instructor = 0; instructor < 5; instructor++)
			{
				if (scheduleArray[instructor].getSlot(day, slot) == SLOT_FREE)
				{
					string tempString = scheduleArray[instructor].getName();
					cout << tempString.substr(0, 1);
				}
				else
					cout << "x";
			}
		}
	}

	cout << endl << "----------------------------------------------------------------------";
	cin.ignore();
	cin.get();
}



void updateAvailableSlotsMenu()
{
	int instructorOption = 0;
	int dayOption = 0;
	int slotOption = 0;
	int instructorAvailabilityOption = 0;

	do //Choose instructor to update their availability
	{
		cout << endl << endl << endl << "Update Availability Menu";
		cout << endl << endl << endl;
		cout << "\tChoose an instructor to update their availability:" << endl << endl;
		cout << "\t\t1. Jeff" << endl;
		cout << "\t\t2. Anna" << endl;
		cout << "\t\t3. Peter" << endl;
		cout << "\t\t4. Michael" << endl;
		cout << "\t\t5. Kerry" << endl;
		cout << "\t\t6. All instructors" << endl;
		cout << "\t\t0. Back" << endl;

		cin >> instructorOption;

		if (instructorOption >= 1 && instructorOption <= 5)
		{			
			cout << endl << endl << endl;
			cout << "\tChoose a day to update its availability:" << endl << endl;
			cout << "\t\t1. Monday" << endl;
			cout << "\t\t2. Tuesday" << endl;
			cout << "\t\t3. Wednesday" << endl;
			cout << "\t\t4. Thursday" << endl;
			cout << "\t\t5. Friday" << endl;
			cout << "\t\t6. Entire week" << endl;
			cout << "\t\t0. Back" << endl;
		
			cin >> dayOption;
		
			if (dayOption >= 1 && dayOption <= 5)
			{					
				cout << endl << endl << endl;
				cout << "\tChoose a slot to update its availability:" << endl << endl;
				cout << "\t\t1. 9am - 10am" << endl;
				cout << "\t\t2. 10am - 11am" << endl;
				cout << "\t\t3. 11am - 12pm" << endl;
				cout << "\t\t4. 1pm - 2pm" << endl;
				cout << "\t\t5. 2pm - 3pm" << endl;
				cout << "\t\t6. 3pm - 4pm" << endl;
				cout << "\t\t7. 4pm - 5pm" << endl;
				cout << "\t\t8. Entire day" << endl;
				cout << "\t\t0. Back" << endl;

				cin >> slotOption;

				//User chooses to change one slot
				if (slotOption >= 1 && slotOption <= 7)
				{
					updateAvailableSlots(instructorOption - 1, dayOption - 1, slotOption - 1);
					return;
				} 

				//User chooses to change one instructor's entire day
				else if (slotOption == 8)
					{
						cout << endl << endl << endl;
						cout << "\tChoose to make whole day free or unavailable:" << endl << endl;
						cout << "\t\t1. Free" << endl;
						cout << "\t\t2. Unavailable" << endl;
						cout << "\t\t0. Back" << endl;
						
						cin >> instructorAvailabilityOption;			
					
						for (int slot = 0; slot < SLOTSINDAY; slot++)
							scheduleArray[instructorOption - 1].setSlot(instructorAvailabilityOption, dayOption - 1, slot);
						return;
					} else cout << endl << "Please enter a valid input";
			}

			//User chooses to change one instructor's entire week
			else if (dayOption == 6)
			{
				cout << endl << endl << endl;
				cout << "\tChoose to make entire week free or unavailable:" << endl << endl;
				cout << "\t\t1. Free" << endl;
				cout << "\t\t2. Unavailable" << endl;
				cout << "\t\t0. Back" << endl;
				
				cin >> instructorAvailabilityOption;			
			
				for (int day = 0; day < DAYSINWEEK; day++)
					for (int slot = 0; slot < SLOTSINDAY; slot++)
						scheduleArray[instructorOption - 1].setSlot(instructorAvailabilityOption, day, slot);
				return;
			} else cout << endl << "Please enter a valid input";
		}

		//User chooses to change every instructor's availability
		else if (instructorOption == 6)
		{
			cout << endl << endl << endl;
			cout << "\tChoose to make all instructors free or unavailable:" << endl << endl;
			cout << "\t\t1. Free" << endl;
			cout << "\t\t2. Unavailable" << endl;
			cout << "\t\t0. Back" << endl;
			
			cin >> instructorAvailabilityOption;

			for (int instructor = 0; instructor < 5; instructor++)			
				for (int day = 0; day < DAYSINWEEK; day++)				
					for (int slot = 0; slot < SLOTSINDAY; slot++)					
						scheduleArray[instructor].setSlot(instructorAvailabilityOption, day, slot);
			return;
		} else cout << endl << "Please enter a valid input";
	} while (instructorOption != 0 && dayOption != 0 && slotOption != 0);
}



void updateAvailableSlots(int instructor, int day, int slot)
{
	switch (scheduleArray[instructor].getSlot(day, slot))
	{
	case SLOT_BOOKED:
		cout << endl << "That slot is currently booked";
		break;

	case SLOT_UNAVAILABLE:
		scheduleArray[instructor].setSlot(SLOT_FREE, day, slot);
		cout << endl << "Selected slot has been made free.";
		break;

	case SLOT_FREE:
		scheduleArray[instructor].setSlot(SLOT_UNAVAILABLE, day, slot);
		cout << endl << "Selected slot has been made unavailable.";
		break;

	default:
		break;
	}
}



void clearScheduleMenu()
{
	int clearScheduleMenuOption = 0;

	do
	{
		cout << endl << endl << endl << "Clear Schedule Menu";
		cout << endl << endl << endl;
		cout << "\tChoose an instructor's schedule to clear:" << endl << endl;
		cout << "\t\t1. Jeff" << endl;
		cout << "\t\t2. Anna" << endl;
		cout << "\t\t3. Peter" << endl;
		cout << "\t\t4. Michael" << endl;
		cout << "\t\t5. Kerry" << endl;
		cout << "\t\t6. All instructor's schedules" << endl;
		cout << "\t\t0. Back" << endl << endl;

		cin >> clearScheduleMenuOption;

		switch (clearScheduleMenuOption)
		{
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
			clearSchedule(clearScheduleMenuOption - 1);
			cout << endl << "Press any key to continue...";
			cin.ignore();
			cin.get();
			break;

		case 6:
			for (int instructor = 0; instructor < 5; instructor++)
			{
				clearSchedule(instructor);						
			}
			cout << endl << "Press any key to continue...";
			cin.ignore();
			cin.get();
			break;

		case 0:
			break;

		default:
			cout << endl << "Please enter a valid input.";
			cin.ignore();
			cin.get();
			break;
		}
	} while (clearScheduleMenuOption != 0);
}



void clearSchedule(int instructor)
{
	for (int slot = 0; slot < SLOTSINDAY; slot++)
	{
		for (int day = 0; day < DAYSINWEEK; day++)
		{
			scheduleArray[instructor].setSlot(SLOT_FREE, day, slot);
		}
	}

	displaySchedule(instructor);
}