//Author:		Alexander Crichton
//Student ID:	n6878296
//Date:			02/04/2012
//Unit:			INB371

#pragma once
#include <iostream>
#include <string>
using namespace std;

const int DAYSINWEEK = 5;
const int SLOTSINDAY = 7;
const int SLOT_FREE = 1;
const int SLOT_BOOKED = 0;
const int SLOT_UNAVAILABLE = 2;

//Objects of type Schedule hold a name and a 2d array
// schedule
class Schedule
{
	private:
		//Class data
		string _name;
		int _schedule[DAYSINWEEK][SLOTSINDAY];

	public:
		//Constructors
		Schedule(void);
		Schedule(string name);

		//Destructor
		~Schedule(void);

		//Accessors
		int getSlot(const int index1, const int index2);
		string getName();

		//Mutators
		void setSlot(const int status, const int index1, const int index2);
};

