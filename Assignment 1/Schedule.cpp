//Author:		Alexander Crichton
//Student ID:	n6878296
//Date:			02/04/2012
//Unit:			INB371

#include "Schedule.h"



//Default Constructor
Schedule::Schedule(void)
{
}



//Cosntructor
Schedule::Schedule(string name)
{
	_name = name;

	for (int i = 0; i < DAYSINWEEK; i++)
		for (int j = 0; j < SLOTSINDAY; j++)
			//_schedule[i][j] = SLOT_FREE;
			_schedule[i][j] = (i%3);	
}



//Default Destructor
Schedule::~Schedule(void)
{
}



//Accessor for schedule
int Schedule::getSlot(const int index1, const int index2)
{
	return _schedule[index1][index2];
}



//Accessor for name
string Schedule::getName()
{
	return _name;
}



//Mutator for schedule
void Schedule::setSlot(const int status, const int index1, const int index2)
{
	_schedule[index1][index2] = status;
}